import 'package:flutter/material.dart';

void main(List<String> args) {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Halo Saya Latihan",
      home: Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () {},
          child: Text("ABC"),
        ),
        // bottomNavigationBar: Container(
        //   height: 50,
        //   child: Text(
        //     "Saya dibawah sendiri",
        //     style: TextStyle(color: Colors.white),
        //   ),
        //   color: Colors.black,
        // ),
        appBar: AppBar(
          title: Text("Halo Saya Latihan"),
          centerTitle: true,
          backgroundColor: Colors.amber,
        ),
        body: SafeArea(
          child: Column(
            children: [
              Text("Saya Widget Di tengah"),
              Container(
                color: Colors.red,
                height: 50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [Text("saya di kiri"), Text("saya di kanan")],
              ),
              Container(
                padding: EdgeInsets.all(10),
                color: Colors.amber,
                width: double.infinity,
                child: Container(
                  height: 50,
                  color: Colors.purple,
                  child: Center(
                    child: Text("Saya Berwarna"),
                  ),
                ),
              ),
              Spacer(),
              Container(
                width: double.infinity,
                height: 100,
                color: Colors.black,
                child: Center(
                  child: Text("saya dibawah sendiri", style: TextStyle(color: Colors.white),),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
