import 'package:flutter/material.dart';

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Asset Latihan"),
      ),
      body: SafeArea(
        child: Stack(
          // alignment: AlignmentDirectional.center,
          children: [
            Container(
              padding: EdgeInsets.all(30),
              height: 400,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(20),
                  bottomLeft: Radius.circular(20),
                ),
                image: DecorationImage(
                    image: AssetImage("assets/beach.jpg"), fit: BoxFit.cover),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text("Hi, David"),
                  Spacer(),
                  CircleAvatar(
                    backgroundImage: AssetImage("assets/badge.png"),
                  )
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
              ),
              margin: EdgeInsets.only(right: 30, left: 30, top: 300),
              height: 200,
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
                    onPressed: () {},
                    child: Text("Halo Button"),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  TextButton(
                    onPressed: () {},
                    child: Text("Pencet Saya"),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  ElevatedButton.icon(
                    style: ButtonStyle(
                        shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ))),
                    onPressed: () {},
                    icon: Icon(Icons.abc),
                    label: Text("dasdasd"),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
