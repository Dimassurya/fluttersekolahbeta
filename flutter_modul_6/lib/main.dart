import 'package:flutter/material.dart';
import 'package:flutter_modul_6/pages/home_page.dart';

void main(List<String> args) {
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Flutter Modul 6",
      home: MyHomePage(),
    );
  }
}